# :wave: *Hi there, I'm Slade!*

[![Discord Presence](https://lanyard.cnrad.dev/api/701886841275547658)](https://discord.com/users/701886841275547658)  

My name is Slade! In my free time, I develop websites, design things for operating systems (how they work, interact with users, etc.), and all sorts of other things. You can find me online at [sladewatkins.com](https://www.sladewatkins.com) and on Mastodon/the fediverse as [@sladewatkins@mastodon.social](https://mastodon.social/@sladewatkins)!

I'm co-founder of the independent group (not a company, yet!) [@Wallymer](https://github.com/Wallymer) alongside one of my best friends, Nat Fox. My responsibilities mainly include being the head & oversight of the group. Additionally, I also work at [Kakariko Herald](https://www.kakarikoherald.com) and help test the [Linux kernel's stable/longterm branches](https://kernel.org/category/releases.html)!

[![ko-fi](https://ko-fi.com/img/githubbutton_sm.svg)](https://ko-fi.com/O4O34KS9A)  
